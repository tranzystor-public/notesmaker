# CMake generated Testfile for 
# Source directory: /home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/apps
# Build directory: /home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/apps
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("annotation")
subdirs("visualisation")
subdirs("interactive-calibration")
subdirs("version")
subdirs("model-diagnostics")
