# CMake generated Testfile for 
# Source directory: /home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/modules/highgui
# Build directory: /home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/modules/highgui
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(opencv_test_highgui "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/bin/opencv_test_highgui" "--gtest_output=xml:opencv_test_highgui.xml")
set_tests_properties(opencv_test_highgui PROPERTIES  LABELS "Main;opencv_highgui;Accuracy" WORKING_DIRECTORY "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/test-reports/accuracy" _BACKTRACE_TRIPLES "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/cmake/OpenCVUtils.cmake;1795;add_test;/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/cmake/OpenCVModule.cmake;1375;ocv_add_test_from_target;/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/modules/highgui/CMakeLists.txt;309;ocv_add_accuracy_tests;/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/modules/highgui/CMakeLists.txt;0;")
