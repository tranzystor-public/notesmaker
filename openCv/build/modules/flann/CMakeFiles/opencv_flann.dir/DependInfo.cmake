
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/modules/flann/src/flann.cpp" "modules/flann/CMakeFiles/opencv_flann.dir/src/flann.cpp.o" "gcc" "modules/flann/CMakeFiles/opencv_flann.dir/src/flann.cpp.o.d"
  "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/modules/flann/src/miniflann.cpp" "modules/flann/CMakeFiles/opencv_flann.dir/src/miniflann.cpp.o" "gcc" "modules/flann/CMakeFiles/opencv_flann.dir/src/miniflann.cpp.o.d"
  )

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/lib/libopencv_flann.so" "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/lib/libopencv_flann.so.4.9.0"
  "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/lib/libopencv_flann.so.409" "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/lib/libopencv_flann.so.4.9.0"
  )


# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/3rdparty/ippiw/CMakeFiles/ippiw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
