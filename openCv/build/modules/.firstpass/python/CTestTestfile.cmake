# CMake generated Testfile for 
# Source directory: /home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/opencv-4.x/modules/python
# Build directory: /home/jan/Documents/Edukacja-2023-2024/zadania-infa/notesMaker/notesmaker/openCv/build/modules/.firstpass/python
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("bindings")
subdirs("test")
subdirs("python2")
subdirs("python3")
