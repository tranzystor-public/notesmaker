#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
int main( int argc, char** argv )
{
    cv::Mat image;
     cv::Mat grayImage;
    image = cv::imread("IMG_2858.png",cv::IMREAD_COLOR);
    if(! image.data)
        {
            std::cout<<"Could not open file" << std::endl;
            return -1;
        }


    cv::cvtColor(image, grayImage, cv::COLOR_RGB2GRAY);

    cv::namedWindow("namba image", cv::WINDOW_NORMAL);
    cv::resizeWindow("namba image",500,500);
    cv::imshow("namba image", grayImage);
    cv::waitKey(0);
    return 0;
}
